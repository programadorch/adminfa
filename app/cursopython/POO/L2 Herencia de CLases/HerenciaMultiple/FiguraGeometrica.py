#ABC  =Abtracta base class
from abc import  ABC, abstractmethod  #importando la clase padre abstracta de python y el decorador para definir un metodo abstracto

# al colocar (ABC) indicamos que la clase actual extiende la clase abtracta ABC
class FiguraGeometrica(ABC):
    def __init__(self,ancho, alto):
        #validacion de datos encabuslados usando la funcion privada _validar_valor de esta misma clase
        if self._validar_valor_(ancho):
            self._ancho = ancho
        else:
            self._ancho = 0
            print(f'Valor de ancho erroneo: {ancho}')
            #---------------
        if self._validar_valor_(alto):
            self._alto = alto
        else:
            self._alto = 0
            print(f'Valor de alto erroneo: {alto}')
    #------ANCHO-------
    @property #accediendo al parametro encpsulado ancho/ GET
    def ancho(self):
        return self._ancho #retornamos el valor
    @ancho.setter #editando el parametro ancho encapsulado/ SET
    def ancho(self, ancho):
        if self._validar_valor_(ancho): #validando nuevo  valor de ancho modificado
            self._ancho = ancho
        else:
            print(f'valor de ancho erroneo: {ancho}')
    #------ALTO-------
    @property #accediendo al parametro encpsulado alto/ GET
    def alto(self):
        return self._alto #retornamos el valor
    @alto.setter #editando el parametro encapsulado alto/ SET
    def alto(self, alto):
        if self._validar_valor_(alto): #validando nuevo  valor de alto modificado
            self._alto = alto
        else:
            print(f'Valor de alto  erroneo : {alto}')

# --------------Agregando metodo Abstracto -------------------------------------------
    @abstractmethod
    def calcular_area(self):
        pass
    # fin del metodo abtracto
    #enviando los valores con str para ser usados en cualquier archivo.py con objetos
    def __str__(self):
        return f'Figura Geometrica [Ancho: {self._ancho} Alto: {self._alto}]'
    #metodo para validar  los adatos encapsulados
    def _validar_valor_(self,valor):
        return True if 0 < valor < 10 else False


