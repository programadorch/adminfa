# =================================================================================
# ========================CLASE PADRE =============================================
# ================================================================================
class Persona:
    #recibiendo argumentos de nombre y edad
    def __init__(self,nombre,edad):
        self.nombre= nombre
        self.edad = edad
        #metodo para enviar parametros en otro archivos
    def __str__(self): #sobreescribiendo metodo str para acceder a los atributos de nombre y edad en otro archivos
        return  f'[Persona; Nombre: {self.nombre }  Edad: {self.edad}]' #retornando valores

#==================================================================================
#=============CLASE HIJA dentro del parentesis va la (clase padre)=================
#==================================================================================
class Empleado(Persona):
    def __init__(self,nombre,edad,sueldo): #nombre y edad son los parametros de la clase padre para usar propiedad user tenemos que ponerlos aqui
        super().__init__(nombre,edad) #propiedad User para acceder a los metodos de la clase Padre
        self.sueldo = sueldo

    def __str__(self):  # sobreescribiendo metodo str para acceder al atributo sueldo
        #llamar a al propiedad user para traer el returno de la clase Padre
        return f'Empleado-{super(). __str__()} [Sueldo: {self.sueldo}]' #retornando valores


