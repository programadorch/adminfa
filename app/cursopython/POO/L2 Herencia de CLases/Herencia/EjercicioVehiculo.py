class Vehiculo:
    def __init__(self, color, ruedas):
        self.color = color
        self.ruedas = ruedas
    def __str__(self):
        return f'color:{self.color} Ruedas: {self.ruedas}'
#----------------------------------------------
class Coche(Vehiculo):
    def __init__( self,color, ruedas, velocidad,):
        super().__init__(color,ruedas) #propiedad User para acceder a los metodos de la clase Padre
        self.velocidad = velocidad
    def __str__(self):
        return f'{super().__str__()}, velocidad (KM/H): {self.velocidad}'
#-------------------------------------------------
class Bicibleta(Vehiculo):
    def __init__(self,color, ruedas, tipo):
        super().__init__(color, ruedas)  # propiedad User para acceder a los metodos de la clase Padre
        self.tipo = tipo
    def __str__(self):
        return f'{super().__str__()}, tipo  {self.tipo}'
#----------OBJETOS--------------------------------------------------------------
DosRuedas = Bicibleta('Rojo','Street','Montaña')
print(f'Bicicleta de caracteristicas: {DosRuedas}')
CuatroRuedas = Coche('Azul','Michellin','800 ')
print(f'Carro de caracteristicas: {CuatroRuedas}')
