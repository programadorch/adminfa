#======================USO DE MODULOS Y CLASES ARCHIVO 1=======================
class Persona: #clase o plantilla
#------iniciando los argumentos -------------------------------------------
    def __init__(self, nombre, apellido, edad ): # argumentos
        # izquierda   atributos   /   derecha  parametros
        self.nombre = nombre # ejemplo:  nombre es --> parametro * que  esta solito encambio self.nombre es un atributo
        self.apellido = apellido
        self.edad = edad
#------------funcion para imprimir los detalles de las personas -----------------------------------------------
    def mostrar_detalle(self):
        print(f'{self.nombre} ,{self.apellido} , {self.edad}') #mostrando atributo nombre del obejto persona

#-------------agregando metodo destructor de objetos  --------------------------------------------------------------------
  #  def __del__(self):
        # mensaje para saber en que momento estamos eliminanado el objeto
       # print(f'Persona: {self.nombre} {self.apellido}')

# ----validadon que solo se ejecute en este archivo y no pueda ser llamado en otros --------imprimiendo datos ---------------------------------------------------------------------------------
if __name__ == '__main__': #__main__ es una propiedad que optiene el nombre de el archivo actual
    # -------------solo se ejecuta este codigo si le damos ejecutar y estamos en este achivo ------------------------------------------
    persona1 = Persona('Carlos', 'Cogua', 30)  # Enviando parametros a el metodo  __init__
    persona1.mostrar_detalle()
    print(__name__)